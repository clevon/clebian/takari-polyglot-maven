Source: takari-polyglot-maven
Section: java
Priority: optional
Maintainer: Raul Tambre <raul@clevon.com>
Vcs-Browser: https://gitlab.com/cleveron/debian/takari-polyglot-maven
Vcs-Git: https://gitlab.com/cleveron/debian/takari-polyglot-maven.git
Rules-Requires-Root: no
Standards-Version: 4.6.2
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 default-jdk-doc <!nodoc>,
 groovy,
 groovy-doc <!nodoc>,
 jruby-maven-plugins,
 junit4 <!nocheck>,
 libcommons-logging-java,
 libcommons-logging-java-doc <!nodoc>,
 libmaven-javadoc-plugin-java <!nodoc>,
 libmaven-plugin-tools-java,
 libmaven3-core-java,
 libplexus-component-annotations-java,
 libplexus-component-metadata-java,
 libplexus-container-default-java,
 libplexus-utils2-java,
 libslf4j-java <!nocheck>,
 maven-debian-helper,

Package: libtakari-polyglot-common-java
Architecture: all
Depends:
 libplexus-component-annotations-java,
 libplexus-container-default-java,
 libplexus-utils2-java,
 ${misc:Depends},
Suggests: libtakari-polyglot-common-java-doc
Replaces: libtakari-polyglot-maven-java
Conflicts: libtakari-polyglot-maven-java
Description: Maven POM model additional language support common code
 Polyglot for Maven is a set of extensions for Maven 3.3.1+ that allows
 the POM model to be written in dialects other than XML.

Package: libtakari-polyglot-common-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: default-jdk-doc, libjs-jquery-ui
Suggests: libtakari-polyglot-common-java
Replaces: libtakari-polyglot-maven-java-doc
Conflicts: libtakari-polyglot-maven-java-doc
Description: Maven POM model additional language support common code (documentation)
 Polyglot for Maven is a set of extensions for Maven 3.3.1+ that allows
 the POM model to be written in dialects other than XML.

Package: libtakari-polyglot-groovy-java
Architecture: all
Depends: groovy, libcommons-logging-java, libtakari-polyglot-common-java, ${misc:Depends}
Suggests: libtakari-polyglot-groovy-java-doc
Replaces: libtakari-polyglot-maven-java
Conflicts: libtakari-polyglot-maven-java
Description: Groovy support for the Maven POM model
 Polyglot for Maven is a set of extensions for Maven 3.3.1+ that allows
 the POM model to be written in dialects other than XML.

Package: libtakari-polyglot-groovy-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: default-jdk-doc, groovy-doc, libcommons-logging-java-doc, libjs-jquery-ui
Suggests: libtakari-polyglot-groovy-java
Replaces: libtakari-polyglot-maven-java-doc
Conflicts: libtakari-polyglot-maven-java-doc
Description: Groovy support for the Maven POM model (documentation)
 Polyglot for Maven is a set of extensions for Maven 3.3.1+ that allows
 the POM model to be written in dialects other than XML.

Package: libtakari-polyglot-maven-plugin-java
Architecture: all
Depends: ${misc:Depends}, libtakari-polyglot-common-java
Suggests: libtakari-polyglot-maven-plugin-java-doc
Description: Maven plugin for generating POMs from other languages
 Polyglot for Maven is a set of extensions for Maven 3.3.1+ that allows
 the POM model to be written in dialects other than XML.

Package: libtakari-polyglot-maven-plugin-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: default-jdk-doc, libjs-jquery-ui
Suggests: libtakari-polyglot-maven-plugin-java
Description: Maven plugin for generating POMs from other languages (documentation)
 Polyglot for Maven is a set of extensions for Maven 3.3.1+ that allows
 the POM model to be written in dialects other than XML.

Package: libtakari-polyglot-ruby-java
Architecture: all
Depends: ${misc:Depends}, libtakari-polyglot-common-java
Suggests: libtakari-polyglot-ruby-java-doc
Description: Ruby support for the Maven POM model
 Polyglot for Maven is a set of extensions for Maven 3.3.1+ that allows
 the POM model to be written in dialects other than XML.

Package: libtakari-polyglot-ruby-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Recommends: default-jdk-doc, libjs-jquery-ui
Suggests: libtakari-polyglot-ruby-java
Description: Ruby support for the Maven POM model (documentation)
 Polyglot for Maven is a set of extensions for Maven 3.3.1+ that allows
 the POM model to be written in dialects other than XML.
